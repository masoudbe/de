### 2.1 Profiling Data

 - read csv data from local to hdfs

 - on command line :
```bash

$ hadoop fs -mkdir -p  /data/sales
$ hadoop fs -mkdir -p /data/clients

```

- on command line 
  - cd to the data folder (/data/Practical Sample/data)
```bash 
$ apt update
$ apt install -y zip
$ unzip raw.zip
```
- cd to the raw folder
```bash	
 $ cd raw
	
-- where the raw sales files exist
$ hadoop fs -put * /data/sales/

-- might get warnings, all is well though
	16/11/10 13:34:00 WARN hdfs.DFSClient: Caught exception 
	java.lang.InterruptedException
	at java.lang.Object.wait(Native Method)
	at java.lang.Thread.join(Thread.java:1281)
	at java.lang.Thread.join(Thread.java:1355)
	at org.apache.hadoop.hdfs.DFSOutputStream$DataStreamer.closeResponder(DFSOutputStream.java:862)
	at org.apache.hadoop.hdfs.DFSOutputStream$DataStreamer.endBlock(DFSOutputStream.java:600)
	at org.apache.hadoop.hdfs.DFSOutputStream$DataStreamer.run(DFSOutputStream.java:789)


	-- where the clients csv file resides
$ cd ..
$ hadoop fs -put clients.csv /data/clients

```
-  run this in hive to install csv serde
- after putting it in the /user/clouder folder in hdfs

```sql
 

-- setup back_office database
create database back_office;
use back_office;

create external table stage_sales (
	RowID string,
	OrderID string,
	OrderDate string,
	OrderMonthYear string,
	Quantity string,
	Quote string,
	DiscountPct string,
	Rate string,
	SaleAmount string,
	CustomerName string,
	CompanyName string,
	Sector string,
	Industry string,
	City string,
	ZipCode string,
	State string,
	Region string,
	ProjectCompleteDate string,
	DaystoComplete string,
	ProductKey string,
	ProductCategory string,
	ProductSubCategory string,
	Consultant string,
	Manager string,
	HourlyWage string,
	RowCount string,
	WageMargin string)
row format serde 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
  "separatorChar" = ",",
  "quoteChar"     = '"',
  "escapeChar"    = "\\"
)
LOCATION '/data/sales/'
tblproperties('skip.header.line.count'='1', 'serialization.null.format'='');


-- $ cd ../lib
-- $ hadoop fs -mkdir /lib
-- $ hadoop fs -put csv-serde-1.1.2-0.11.0-all.jar /lib

-- -- add jar hdfs:///lib/csv-serde-1.1.2-0.11.0-all.jar;
-- or 
--<property>
--  <name>hive.aux.jars.path</name>
--  <value>hdfs:///lib/csv-serde-1.1.2-0.11.0-all.jar</value>
-- </property>
-- create audit table

```