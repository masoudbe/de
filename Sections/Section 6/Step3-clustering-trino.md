#### Using lewuathe Images

```dockerfile
version: '3'

services:
  coordinator:
    image: "lewuathe/trino-coordinator:352"
    ports:
      - "8080:8080"
    container_name: "coordinator"
    command: http://coordinator:8080 coordinator
    volumes:
      - ./catalog:/usr/local/trino/etc/catalog
  worker0:
    image: "lewuathe/trino-worker:352"
    container_name: "worker0"
    ports:
      - "8081:8081"
    command: http://coordinator:8080 worker0
    volumes:
      - ./catalog:/usr/local/trino/etc/catalog
  worker1:
    image: "lewuathe/trino-worker:352"
    container_name: "worker1"
    ports:
      - "8082:8081"
    command: http://coordinator:8080 worker1
    volumes:
      - ./catalog:/usr/local/trino/etc/catalog
  worker1:
    image: "lewuathe/trino-worker:352"
    container_name: "worker2"
    ports:
      - "8082:8081"
    command: http://coordinator:8080 worker2
    volumes:
      - ./catalog:/usr/local/trino/etc/catalog
      
  mariadb:
    image: mariadb:latest
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_USER: admin
      MYSQL_PASSWORD: admin
      MYSQL_DATABASE: metastore_db


  # hive-metastore:
  #   build: .
  #   image: hive-metastore:latest
  #   ports:
  #   - 9083:9083
  #   depends_on:
  #     - mariadb

```

