from airflow import DAG
from airflow.models.taskinstance import TaskInstance
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.hooks.base_hook import BaseHook
from airflow import AirflowException
from datetime import timedelta, datetime
from os import path
import logging
from airflow.operators.slack_operator import SlackAPIPostOperator
import time
from textwrap import dedent
from typing import Any, Dict, List, Optional, Tuple



default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2020, 1, 1, 23, 15, 0),
    'email': None,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0
}



EXCEL_FILE_PATH = "/tmp"
EXCEL_FILE_EXT = "xlsx"

with DAG('sla_dag', default_args=default_args, schedule_interval="*/1 * * * *", catchup=False) as dag:

    t0 = DummyOperator(task_id='final_task')

    # t1 = BashOperator(task_id='t1', bash_command='sleep 15', sla=timedelta(seconds=5), retries=0)

    task_read_stock_exchange_xlsx_file = BashOperator(
        task_id='Download-Stock-Exchange-Xlsx-File_1',
        bash_command='curl --retry 10 --output {0} -L -H "User-Agent:Chrome/61.0" --compressed "http://members.tsetmc.com/tsev2/excel/MarketWatchPlus.aspx?d=0"'.format(
            path.join(EXCEL_FILE_PATH, "{0}_{1}.{2}".format('daily_trades',  datetime.now().strftime("%Y_%m_%d_%H_%M_%S"), EXCEL_FILE_EXT))),
        sla=timedelta(seconds=5),
    )



    task_read_stock_exchange_xlsx_file >> t0
