# ElasticsearchLink = MenuLink(category='Elasticsearch Plugin', name='More Info', url='https://marclamberti.com')

# Creating a flask appbuilder Menu Item
appbuilder_mitem = {"name": "Kibana - Latest Tweets ",
                    "category": "Quick Access",
                    "category_icon": "fa-th",
                    "href": "http://localhost:5601/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(_source),filters:!(),index:'537bf370-7d2b-11eb-b228-bfe78817db29',interval:auto,query:(language:kuery,query:''),sort:!())"}