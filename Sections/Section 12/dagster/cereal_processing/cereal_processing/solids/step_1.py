import csv
import os

from dagster import pipeline, solid

@solid
def hello_cereal(context):
    # Assumes the dataset is in the same directory as this Python file
    dataset_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}cereal.csv")
    context.log.info(f"{dataset_path}")
    with open(dataset_path, "r") as fd:
        # Read the rows in using the standard csv library
        cereals = [row for row in csv.DictReader(fd)]

    context.log.info(f"Found {len(cereals)} cereals")