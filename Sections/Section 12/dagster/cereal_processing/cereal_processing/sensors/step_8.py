import os
from dagster import sensor, RunRequest


@sensor(pipeline_name="step4_pipeline", mode='test')
def new_cereals(_context):
    csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data")

    for filename in os.listdir(csv_path):
        filepath = os.path.join(csv_path, filename)
        # _context.log.info(f"File Name : {filepath}")
        if os.path.isfile(filepath):
            yield RunRequest(
                run_key=filename,
                run_config={"solids": {"read_csv": {"config": {"csv_name": filename} } } }
            )