#### Install Meltano

---

Install Meltano On WSL or Use Linux Or Melatno Docker Image

---



```bash
$ wsl
$ cd 
$ mkdir meltano && cd meltano
$ python3 -m venv .venv-meltano
$ source .\.venv-meltano\bin\activate
$ python3 -m pip install --upgrade pip
$ pip3 install meltano
```

#### Init a sample Project

```bash
$ meltano init sahamyab-etl
$ cd sahamyab-etl
$ meltano ui
......
Meltano UI is now available at http://localhost:5000
```

- make a simple pipeline : ***Mongo Tweets to A JSON File*** 
  - mkdir -p `data_lake/tweets`
  - `meltano config tap-mongodb set _metadata tweets-sahamyab replication-method FULL_TABLE`
  - `meltano config tap-mongodb set _metadata tweets-sahamyab selected true`
  - `meltano config tap-mongodb set _metadata tweets-delete_me selected false` 

#### Add tap-sahamyab

- copy `tap-sahamyab` folder to `sahamyab-etl/extract`

```bash
$ meltano add --custom extractor tap-sahamyab
...
(namespace) [tap_sahamyab]:
....
(pip_url) [tap-sahamyab]: -e extract/tap-sahamyab
...
(executable) [tap-sahamyab]:
...
(capabilities) [[]]:
...
(settings) [[]]:
...
```



#### Add Target-CSV-Dev

- push target-csv-dev to github

  ```bash
  $ meltano add --custom loader target-csv-unicode
  ...
  (namespace) [target_csv_unicode]:
  ...
  (pip_url) [target-csv-unicode]: git+https://github.com/dbrg-ut/target-csv.git
  ...
  (executable) [target-csv]:
  ...
  (settings) [[]]:
  ...
  $ meltano config target-csv-unicode set destination_path csv
  ```

 - go to meltano ui and create a pipeline

   

#### Add Airflow

   ```bash
   $ meltano add orchestrator airflow
   ....
   $ meltano invoke airflow scheduler -D
   ....
   $ meltano invoke airflow webserver
   ....
   
   
   ```

-  check airflow in browser

#### Sample meltano.yml

 - CLI or Manually Edit file

   ```yaml
   version: 1
   send_anonymous_usage_stats: true
   project_id: 2059085b-f792-41a7-acf2-8c0267bb732e
   plugins:
     extractors:
     - name: tap-mongodb
       variant: singer-io
       pip_url: tap-mongodb
       config:
         user: nikamooz
         host: mongodb3
         port: 21362
         replica_set: nikamoozreplica
         database: admin
     loaders:
     - name: target-jsonl
       variant: andyh1203
       pip_url: target-jsonl
   ```

   

####  Create Catalog

-  ```bash
  meltano elt tap-mongodb target-jsonl --job_id=mongo-to-jsonl --catalog=catalogs/mongo-catalog.json --dump=catalog > catalogs/mongo-catalog.json
  ```
  
  - set "*selected : true*" only for desired tables in catalog file
  - set "*replication-method*"
  - set other fields

#### Run the Pipeline

- ```bash
  meltano elt tap-mongodb target-jsonl --job_id=mongo-to-jsonl --catalog=catalogs/mongo-catalog.json
  ```

#### Check the UI

- `meltano ui`



