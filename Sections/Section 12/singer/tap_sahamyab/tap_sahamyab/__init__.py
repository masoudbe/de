#!/usr/bin/env python3
import os
import json
import singer
from singer import utils, metadata
from singer.catalog import Catalog, CatalogEntry
from singer.schema import Schema
import urllib.request
import argparse
import datetime
import sys

sys.stdout.reconfigure(encoding='utf-8')

DATE_FORMAT="%Y-%m-%d"

REQUIRED_CONFIG_KEYS = []

schema_tweet = {
    'type': 'object',
    'properties': {
        'id': {'type': 'string'},
        'senderName': {'type': 'string'},
        'senderUsername': {'type': 'string'},
        'sendTime': {'type': 'string'},
        'sendTimePersian': {'type': 'string'},
        'type': {'type': 'string'},
        'content': {'type': 'string'}
    }
}


LOGGER = singer.get_logger()


def get_abs_path(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)


def sync(config, state, schema):
    """ Sync data from tap source """
    # Loop over selected streams in catalog
    schema = schema
    config=config
    state=state

    try : 
        singer.write_schema("sahamyab",schema,"id")
        url = "https://www.sahamyab.com/guest/twiter/list?v=0.1"
        hdr = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)' }
        req = urllib.request.Request(url, headers=hdr)
        response = urllib.request.urlopen(req)
        tweet_data = json.loads(response.read().decode('utf-8'))
        for tweet in tweet_data["items"] :
            try : 
                record = {}
                record["id"] = tweet["id"]
                record["senderName"] = tweet["senderName"]
                record["senderUsername"] = tweet["senderUsername"]
                record["sendTime"] = tweet["sendTime"]
                record["sendTimePersian"] = tweet["sendTimePersian"]
                record["type"] = tweet["type"]
                record["content"] = tweet["content"]
                #### stream=sahamyab & schema=sahamyab
                singer.write_records("sahamyab", [record])
                LOGGER.info(f"tweet with id : {record['id']} from {record['senderUsername']} was sent to stdout")
            except : 
                continue
    except : 
        sys.exit(-1)

    return


@utils.handle_top_exception(LOGGER)
def main():
    # Parse command line arguments
 ######################################   
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-c", "--config", help="Config file", required=False)
    parser.add_argument(
        "-s", "--state", help="State file", required=False)
    parser.add_argument(
        "-m", "--schema", help="State file", required=False)


    args = parser.parse_args()

    if args.config:
        with open(args.config) as f:
            config = json.load(f)
    else:
        config = {}

    if args.state:
        with open(args.state) as f:
            state = json.load(f)
    else:
        state = {}

    if args.schema:
        with open(args.schema) as f:
            schema = json.load(f)
    else:
        schema = schema_tweet

    # start_date = (state.get("start_date") or config.get("start_date") or
    #               datetime.datetime.utcnow().strftime(DATE_FORMAT))

    sync(config, state, schema)


if __name__ == "__main__":
    main()
